import { Injectable } from '@angular/core';
import { OAuthService, AuthConfig } from 'angular-oauth2-oidc';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';

export const authCodeFlowConfig: AuthConfig = {
  issuer: environment.issuerUrl,
  clientId: 'waecm',
  responseType: 'id_token token',
  redirectUri: window.location.origin + '/login',
  postLogoutRedirectUri: window.location.origin + '/login',
  scope: 'openid profile'
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  username: string;

  constructor(
    private oauthService: OAuthService,
    private http: HttpClient
  ) {
    this.oauthService.setStorage(localStorage);
   }

  async initOpenId(): Promise<boolean> {

    this.oauthService.configure(authCodeFlowConfig);

    return this.oauthService.loadDiscoveryDocument()
      .then(() => this.oauthService.tryLogin()).catch( error => {
        // return Promise.reject(error);
        // console.log(error)
        // Trigger pipeline 2
        return null;
      });

  }

  isLoggedIn(): boolean {
    return (this.validAccessToken() && this.validIdToken());
  }

  login() {
    if (!this.oauthService.hasValidIdToken || !this.oauthService.hasValidAccessToken()) {
      this.oauthService.customQueryParams = { nonce: this.createAndGetNonce(), prompt: 'consent' };
      this.oauthService.initImplicitFlow();
    }
  }

  logout() {
    this.oauthService.logOut();
  }

  validIdToken(): boolean {
    return this.oauthService.hasValidIdToken();
  }

  validAccessToken(): boolean {
    return this.oauthService.hasValidAccessToken();
  }

  getIdToken() {
    return this.oauthService.getIdToken();
  }

  // get user info -- uses proxy.conf.json to access user endpoint without cors
  fetchUserInfoFromAccessToken(): Observable<User> {
    const atkn = 'Bearer ' + this.oauthService.getAccessToken();
    return this.http.get<User>('userinfo', { headers: {Authorization: atkn }});
  }

  getUserNameFromIdToken() {
    const json = this.oauthService.getIdentityClaims();
    return json['name'];
  }

  createAndGetNonce() {
    const currNonce = localStorage.getItem('nonce');

    if (currNonce == null) {
      this.oauthService.createAndSaveNonce().then( (nonce) => {
          return nonce;
      });
    }
  }
}
