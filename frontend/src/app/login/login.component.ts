import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { BackendService } from '../backend.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logo = 'https://logodix.com/logo/767267.png';

  constructor(
    private authService: AuthService,
    private backendService: BackendService,
    private router: Router
  ) { }

  async ngOnInit() {
    await this.authService.initOpenId();

    if (this.authService.validIdToken()) {
      this.backendService.validateToken().subscribe( (resp) => {
        if ( resp.status === 200 ) {
          this.router.navigate(['/dashboard']);
        }
      });
    }
  }

  login() {
    this.authService.login();
  }
}
