import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { Subreddit } from '../models/subreddit';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dataSourceSubreddits = new MatTableDataSource<Subreddit>();
  columnsSubreddits = ['srImage', 'srName', 'srDesc', 'srCnt'];

  constructor(
    private backendService: BackendService
  ) { }

  ngOnInit() {
    this.backendService.getSubredditList().subscribe( (list) => {
      this.dataSourceSubreddits.data = list;
    });
  }

  testBackend() {
    this.backendService.isBackendAlive().subscribe( (data) => alert(data));
  }
}
