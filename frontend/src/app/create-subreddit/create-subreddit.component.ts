import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';
import { Subreddit } from '../models/subreddit';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create-subreddit',
  templateUrl: './create-subreddit.component.html',
  styleUrls: ['./create-subreddit.component.css']
})
export class CreateSubredditComponent implements OnInit {

  public entryForm: FormGroup;
  subreddit: string;
  keywords: string;
  answer: string;
  active: boolean;

  constructor(
    private backendService: BackendService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.entryForm = new FormGroup({
      subreddit: new FormControl('', [Validators.required]),
      keywords: new FormControl('', [Validators.required]),
      answer: new FormControl('', [Validators.required]),
      active: new FormControl(false)
    });

  }

  onSave(entryFormValue: any) {
    if (this.entryForm.valid) {

      const sr = new Subreddit();
      sr.subreddit_name = entryFormValue.subreddit;
      sr.keywords = entryFormValue.keywords;
      sr.isActive = entryFormValue.active;
      sr.answer = entryFormValue.answer;

      this.backendService.createSubreddit(sr).subscribe( (subreddit) => {
        console.log(subreddit);

        // TODO snackbar only on correct createino
        this.snackBar.open('Subreddit erstellt', '', {
          duration: 2000,
          verticalPosition: 'top'
        });
      });

    }
  }

  onCancel() {
    this.router.navigate(['/dashboard']);
  }

}
