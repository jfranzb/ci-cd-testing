import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subreddit } from './models/subreddit';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  isBackendAlive(): Observable<string> {
    return this.http.get(environment.backendUrl + '/test', { responseType: 'text' });
  }

  validateToken(): Observable<HttpResponse<object>> {
    return this.http.get<HttpResponse<object>>(environment.backendUrl + '/validate',
    { observe: 'response' });
  }

  createSubreddit(data: Subreddit): Observable<Subreddit> {
    return this.http.post<Subreddit>(environment.backendUrl + '/entry', data);
  }

  updateSubreddit(): Observable<boolean> {
    // "/entry/{id}"
    return this.http.put<boolean>(environment.backendUrl + '/...', { responseType: 'text' });
  }

  removeSubreddit(): Observable<boolean> {
    // "/entry/{id}"
    return this.http.delete<boolean>(environment.backendUrl + '/...');

  }

  getSubredditList(): Observable<Subreddit[]> {
    return this.http.get<Subreddit[]>(environment.backendUrl + '/allEntries');
  }
}
