export class Subreddit {
    id?: number;
    subreddit_name: string;
    subreddit_description?: string;
    keywords: string;
    answer: string;
    counter?: number;
    subreddit_image?: string;
    isActive: boolean;
}
