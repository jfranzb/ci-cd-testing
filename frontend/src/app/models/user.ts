export class User {
    sub: string;
    name: string;
    nickname: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    picture: string;
}
