package com.example.microservice_a

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MicroserviceAApplication

fun main(args: Array<String>) {
	runApplication<MicroserviceAApplication>(*args)
}
