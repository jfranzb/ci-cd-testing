package com.example.microservice_a

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class MicroserviceAApplicationTests {

	@Test
	fun contextLoads() {
		assertEquals("Hi Harry and Ron and Hermione", "Hi Harry and Ron and Hermione")
	}

	@Test
	fun contextLoads2() {
		assertEquals("Hi Harry and Ron and Hermione", "Hi Harry and Ron and Hermione")
	}

}
